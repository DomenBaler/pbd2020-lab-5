package si.uni_lj.fri.pbd.lab5;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class TimerService extends Service {

    private static final String TAG = TimerService.class.getSimpleName();

    private static final int NOTIFICATION_ID = 22;

    public static final String ACTION_STOP = "stop_service";

    public static final String ACTION_START = "start_service";

    private static final String channelID = "background_timer";

    private final IBinder serviceBinder = new RunServiceBinder();

    private long startTime, endTime;

    private boolean isTimerRunning;

    // TODO: define a static final int NOTIFICATION_ID

    // TODO: move startTime, endTime, isTimerRunning from the MainActivity

    // TODO: Define serviceBinder and instantiate it to RunServiceBinder

    @Override
    public void onCreate() {
        Log.d(TAG, "Creating service");

        isTimerRunning = false;

        startTime = 0;

        endTime = 0;


        // TODO: set startTime, endTime, isTimerRunning to default values

        // TODO: create notification channel

        createNotificationChannel();
    }

    public void startTimer() {
        if (!isTimerRunning) {
            startTime = System.currentTimeMillis();
            isTimerRunning = true;
        }
        else {
            Log.e(TAG, "startTimer request for an already running timer");
        }
    }

    public long elapsedTime() {
        // If the timer is running, the end time will be zero
        return endTime > startTime ?
                (endTime - startTime) / 1000 :
                (System.currentTimeMillis() - startTime) / 1000;
    }


    /**
     * Stops the timer
     */
    public void stopTimer() {
        if (isTimerRunning) {
            endTime = System.currentTimeMillis();
            isTimerRunning = false;
        }
        else {
            Log.e(TAG, "stopTimer request for a timer that isn't running");
        }
    }


    /**
     * @return whether the timer is running
     */
    public boolean isTimerRunning() {
        return isTimerRunning;
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting service");

        if(intent.getAction() == ACTION_STOP){
            stopForeground(true);
            stopSelf();
        }

        // TODO: check the intent action and if equal to ACTION_STOP, stop the foreground service

        return Service.START_STICKY;
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");

        // TODO: Return serviceBinder
        return serviceBinder;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destroying service");
    }


    public class RunServiceBinder extends Binder {
        TimerService getService(){
            return TimerService.this;
        }
    }

    public void foreground(){
        startForeground(NOTIFICATION_ID,createNotification());
    }

    public void background(){
        stopForeground(true);
    }

    /**
     * Creates a notification for placing the service into the foreground
     *
     * @return a notification for interacting with the service when in the foreground
     */
    // TODO: Uncomment for creating a notification for the foreground service
     private Notification createNotification() {

        // TODO: add code to define a notification action

         Intent actionIntent = new Intent(this, TimerService.class);
         actionIntent.setAction(ACTION_STOP);
         PendingIntent actionPendingIntent = PendingIntent.getService(this,0,actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelID)
                .setContentTitle(getString(R.string.notif_title))
                .setContentText(getString(R.string.notif_text))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)
                .addAction(android.R.drawable.ic_media_pause,"Stop",actionPendingIntent);

        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0, resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        return builder.build();
    }

    // TODO: Uncomment for creating a notification channel for the foreground service
     private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT < 26) {
            return;
        } else {

            NotificationChannel channel = new NotificationChannel(TimerService.channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(getString(R.string.channel_desc));
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);

            NotificationManager managerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            managerCompat.createNotificationChannel(channel);
        }
    }
}
